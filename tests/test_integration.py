#!/usr/bin/env python3
# coding=utf-8
import argparse
import shutil
from pathlib import Path

import pytest

from thingsboardbackup.__main__ import main


@pytest.fixture()
def args(tmp_path: Path) -> argparse.Namespace:
    args = argparse.Namespace()
    args.type = "dashboards"
    args.directory = tmp_path
    args.verbose = 1
    args.config = Path(tmp_path, "test_config.cfg")
    return args


def test_basic_dashboards(args: argparse.Namespace):
    shutil.copy("tests/test_config.cfg", args.directory)
    main(args)
