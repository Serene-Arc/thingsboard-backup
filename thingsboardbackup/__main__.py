#!/usr/bin/env python3
# coding=utf-8

import argparse
import configparser
import logging
import sys
import time
from pathlib import Path
from urllib.parse import urljoin

from selenium.webdriver import Firefox, FirefoxProfile, Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.support.wait import WebDriverWait

parser = argparse.ArgumentParser()
logger = logging.getLogger()


def _setup_logging(verbosity: int):
    logger.setLevel(1)
    stream = logging.StreamHandler(sys.stderr)
    formatter = logging.Formatter("[%(asctime)s - %(name)s - %(levelname)s] - %(message)s")
    stream.setFormatter(formatter)
    logger.addHandler(stream)

    if verbosity > 0:
        stream.setLevel(logging.DEBUG)
    else:
        stream.setLevel(logging.INFO)


def add_arguments(parser: argparse.ArgumentParser):
    parser.add_argument("-v", "--verbose", action="count", default=0)
    parser.add_argument("-c", "--config", default="backup.cfg")
    parser.add_argument("directory", default=".")
    parser.add_argument("-t", "--type", choices=["dashboards", "devices"])


def main(args: argparse.Namespace):
    _setup_logging(args.verbose)
    args.config = Path(args.config).expanduser().resolve()
    args.directory = Path(args.directory).expanduser().resolve()
    if not args.config.exists():
        logger.critical(f"Configuration file was not found at {args.config}")
        sys.exit(1)
    if not args.directory.exists():
        logger.warning(f"Creating directory structure at {args.directory}")
        args.directory.mkdir(parents=True)
    config = configparser.ConfigParser()
    config.read(args.config)
    options = Options()
    options.headless = True
    profile = FirefoxProfile()
    profile.set_preference("browser.download.folderList", 2)
    profile.set_preference("browser.download.manager.showWhenStarting", False)
    profile.set_preference("browser.download.dir", args.directory)
    profile.set_preference("browser.helperApps.neverAsk.saveToDisk", "*/*")
    with Firefox(options=options) as driver:
        url = config.get("DEFAULT", "url")
        username = config.get("DEFAULT", "username")
        password = config.get("DEFAULT", "password")
        driver.get(url + "/login")
        e = WebDriverWait(driver, 20).until(lambda x: x.find_element(By.ID, "username-input"))
        e.send_keys(username)
        e = driver.find_element(By.ID, "password-input")
        e.send_keys(password)
        e.send_keys(Keys.RETURN)
        time.sleep(3)
        if "home" not in driver.current_url:
            logger.critical("Login has failed")
            sys.exit(1)
        driver.get(url + "/" + args.type)
        time.sleep(3)
        elements = driver.find_elements(By.TAG_NAME, "button")
        for e in elements:
            print(e.get_dom_attribute("aria-describedby"))
            # if e.get_attribute('aria-describedby') == 'cdk-describedby-message-64':
            # e.click()


def entry():
    add_arguments(parser)
    args = parser.parse_args()
    main(args)


if __name__ == "__main__":
    entry()
